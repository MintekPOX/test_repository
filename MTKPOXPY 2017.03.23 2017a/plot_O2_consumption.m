function plot_O2_consumption(location,time,O2_consumption,sTitle,sColour,sDispName)

    subplot(2,2,location);
%     figure(FigName)
    plot(time,O2_consumption,'Color',sColour,'Linewidth',1.5,'DisplayName',sDispName); hold on;
    xlabel('time (seconds)'); ylabel('oxygen mass (kg)');
    xlim([0 1000]);
    title(sTitle);  
    

end