function plot_PSDs(x,y,z_SiO2,z_FeS2,stitle)
    figure
    ax = axes;
    h = surf(x,y,z_SiO2);
    set(h,'LineStyle','none')
    alpha 0.5
    hold on;
    h2 = surf(x,y,z_FeS2);
    set(h2,'LineStyle','none')
    set(ax,'Ydir', 'reverse')
    set(ax,'Xdir', 'reverse')
    ylim([0 10000])
    xlim([0 350])
    ylabel('time (s)');
    xlabel('Particle Size (um)')
    zlabel('Mass kg (abs)')
%     xticklabels({'-3\pi','-2\pi','-\pi','0','\pi','2\pi','3\pi'})
%     set(gca, 'XScale', 'log')
    grid off
    title(sprintf(stitle));
end