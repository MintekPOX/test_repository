function plot_solution_conc(location,tout,H2SO4,Fe2SO43,O2,FeSO4,MgSO4,stitle)

subplot(2,2,location)
yyaxis right; plot(tout,O2,'b-'); hold on;
yyaxis right; plot(tout,FeSO4,'m-'); hold on;
yyaxis right; plot(tout,MgSO4,'c-'); hold on;
yyaxis right;  set(gca,'ycolor','k')
yyaxis right; ylabel('Concentration (g/L)')

yyaxis left;  plot(tout,H2SO4,'g-'); hold on;
yyaxis left;  plot(tout,Fe2SO43,'y-'); hold on;

yyaxis left; set(gca,'ycolor','k')
yyaxis left; ylabel('Concentration (g/L)');

xlabel('time (seconds)'); 
legend('\leftarrow H2SO4','\leftarrow Fe2(SO4)3','O2 \rightarrow','FeSO4 \rightarrow','MgSO4 \rightarrow'); 
title(stitle)

end