function plot_TempAndPress(location, time,temperature,pressure,sTitle)
    
    subplot(2,2,location);
    yyaxis left; plot(time,pressure); ylabel('Pressure (bar)'); hold on;
    yyaxis right; plot(time,temperature); ylabel('Temperature (oC)'); ylim([193 206]); hold on;
    xlabel('time (seconds)');
    title(sTitle);  
    
end