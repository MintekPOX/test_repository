function plot_extents(location,time,extent,sTitle,sColour,sDispName)

    subplot(2,2,location);
%     figure(FigName)
    plot(time,extent,'Color',sColour,'Linewidth',1.5,'DisplayName',sDispName); hold on;
    xlabel('time (seconds)'); ylabel('Fraction of pyrite oxidation extent');
%     xlim([0 1000]);
    title(sTitle);  
    

end