function plot_Temp(location,time,temperature,sTitle,sColour,sDispName)
    
    subplot(2,2,location);
%     figure(FigName)
    plot(time,temperature,'Color',sColour,'Linewidth',1.5,'DisplayName',sDispName); hold on;
    ylabel('Temperature (oC)'); xlabel('time (seconds)');
     ylim([199 203]); 
%     xlim([0 1000])
    title(sTitle);  
    
end