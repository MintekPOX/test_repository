function plot_solution_acid(location,time,acid,sTitle,sColour,sDispName)

    subplot(2,2,location);
%     figure(FigName)
    plot(time,acid,'Color',sColour,'Linewidth',1.5,'DisplayName',sDispName); hold on;
    xlabel('time (seconds)'); ylabel('H2SO4 concentration (g/L)');
%     xlim([0 1000]);
    title(sTitle);  
    

end