function plot_solution_Fe2(location,time,Fe2,sTitle,sColour,sDispName)

    subplot(2,2,location);
%     figure(FigName)
    plot(time,Fe2,'Color',sColour,'Linewidth',1.5,'DisplayName',sDispName); hold on;
    xlabel('time (seconds)'); ylabel('FeSO4 concentration (g/L)');
%     xlim([0 1000]);
    title(sTitle);  
    

end