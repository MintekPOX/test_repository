%% plot temperature & pressure in each pot

figure;
plot_TempAndPress(1,tout,simlog.Autoclave_Pot_1.Pressure_Reactor.T.series.values-273.15,(1e-5)*simlog.Autoclave_Pot_1.Pressure_Reactor.P.series.values,'Pot1');
plot_TempAndPress(2,tout,simlog.Autoclave_Pot_2.Pressure_Reactor.T.series.values-273.15,(1e-5)*simlog.Autoclave_Pot_2.Pressure_Reactor.P.series.values,'Pot2');
plot_TempAndPress(3,tout,simlog.Autoclave_Pot_3.Pressure_Reactor.T.series.values-273.15,(1e-5)*simlog.Autoclave_Pot_3.Pressure_Reactor.P.series.values,'Pot3');
plot_TempAndPress(4,tout,simlog.Autoclave_Pot_4.Pressure_Reactor.T.series.values-273.15,(1e-5)*simlog.Autoclave_Pot_4.Pressure_Reactor.P.series.values,'Pot4');


%% plot particle size distributions
x = ((exp(x1))*1e6);
y = tout;

% residue = reshape(simlog.Autoclave_Pot_1.Pressure_Reactor.m.series.values,[],length(tout))';
% z_SiO2 = residue(:,1).*reshape(simlog.Autoclave_Pot_1.Pressure_Reactor.fDm_1_meas.series.values,[],length(tout))';
% z_FeS2 = residue(:,8).*reshape(simlog.Autoclave_Pot_1.Pressure_Reactor.fDm_8_meas.series.values,[],length(tout))';
% plot_PSDs(x,y,z_SiO2,z_FeS2,'PSDs pot 1')
% 
% residue = reshape(simlog.Autoclave_Pot_2.Pressure_Reactor.m.series.values,[],length(tout))';
% z_SiO2 = residue(:,1).*reshape(simlog.Autoclave_Pot_2.Pressure_Reactor.fDm_1_meas.series.values,[],length(tout))';
% z_FeS2 = residue(:,8).*reshape(simlog.Autoclave_Pot_2.Pressure_Reactor.fDm_8_meas.series.values,[],length(tout))';
% plot_PSDs(x,y,z_SiO2,z_FeS2,'PSDs pot 2')
% 
% residue = reshape(simlog.Autoclave_Pot_3.Pressure_Reactor.m.series.values,[],length(tout))';
% z_SiO2 = residue(:,1).*reshape(simlog.Autoclave_Pot_3.Pressure_Reactor.fDm_1_meas.series.values,[],length(tout))';
% z_FeS2 = residue(:,8).*reshape(simlog.Autoclave_Pot_3.Pressure_Reactor.fDm_8_meas.series.values,[],length(tout))';
% plot_PSDs(x,y,z_SiO2,z_FeS2,'PSDs pot 3')

residue = reshape(simlog.Autoclave_Pot_4.Pressure_Reactor.m.series.values,[],length(tout))';
z_SiO2 = residue(:,1).*reshape(simlog.Autoclave_Pot_4.Pressure_Reactor.fDm_1_meas.series.values,[],length(tout))';
z_FeS2 = residue(:,8).*reshape(simlog.Autoclave_Pot_4.Pressure_Reactor.fDm_8_meas.series.values,[],length(tout))';
plot_PSDs(x,y,z_SiO2,z_FeS2,'PSDs pot 4')

%% plot extents
figure
plt = gca;
yyaxis left; plot(Extents_A2.Time,Extents_A2.Data,'k:','Linewidth',2); hold on
yyaxis left; plot(Extents_A3.Time,Extents_A3.Data,'k--'); hold on
yyaxis left; plot(Extents_A4.Time,Extents_A4.Data,'k'); hold on
grid on
xlabel('Time (s)')
yyaxis left; ylabel('Pyrite oxidation extent (fraction)')


%% plot O2 molalities
liq_molalities_pot1 = reshape(simlog.Autoclave_Pot_1.Pressure_Reactor.my_meas.series.values,9,[]);
liq_molalities_pot2 = reshape(simlog.Autoclave_Pot_2.Pressure_Reactor.my_meas.series.values,9,[]);
liq_molalities_pot3 = reshape(simlog.Autoclave_Pot_3.Pressure_Reactor.my_meas.series.values,9,[]);
liq_molalities_pot4 = reshape(simlog.Autoclave_Pot_4.Pressure_Reactor.my_meas.series.values,9,[]);
yyaxis right; plot(tout,liq_molalities_pot2(1,:),'b:','Linewidth',2); hold on
yyaxis right; plot(tout,liq_molalities_pot3(1,:),'b--'); hold on
yyaxis right; plot(tout,liq_molalities_pot4(1,:),'b-'); hold on
yyaxis right; ylabel('[O2] molalities')
yyaxis right; ylim([-0.004, 0.04])
title('Mineral Extractions/[O2] Molalities')
legend('Pot2 Extent','Pot3 Extent','Pot4 Extent','Pot2 O2','Pot3 O2','Pot4 O2')
plt.YAxis(1).Color = 'k';
plt.YAxis(2).Color = 'b';


%% plot Solution Tenors
figure
yyaxis left; plot(Tenors_A5.Time,Tenors_A5.Data(:,1),'b'); hold on;
yyaxis right; plot(Tenors_A5.Time,Tenors_A5.Data(:,5),'r--'); hold on;
yyaxis right; plot(Tenors_A5.Time,Tenors_A5.Data(:,6),'r-'); hold on;
legend('H2SO4','Fe(II)','Fe(III)')
xlabel('Time (s)')
yyaxis left; ylabel('Acid (g/L)'); ylim([0 70])
yyaxis right; ylabel('Fe (g/L)'); ylim([0 1])
title('Solution Tenors (After Flashing)')

%% plot valve movements
figure;
subplot(2,1,1)
plot(tout,simlog.Autoclave_Pot_4.Pressure_Reactor.L_meas.series.values); hold on;
plot(tout,repmat(L_sl_oG_A4,1,length(tout)),'r-');
legend('measured','setpoint')
xlabel('time (seconds)'); ylabel('Level (m)')
title('Level control')
subplot(2,1,2)
plot(tout,(1e-5)*simlog.Autoclave_Pot_4.Pressure_Reactor.P.series.values); hold on;
plot(tout,(1e-5)*repmat(Pg+P_atm,1,length(tout)),'r-');
legend('measured','setpoint')
xlabel('time (seconds)'); ylabel('Pressure (bar)')
title('Pressure control')

figure;
subplot(2,1,1)
% plot(tout,simlog.Slurry_Valve2.Slurry_Valve.fvai.series.values);          %Example_2
% plot(tout,simlog.Flash_Valve1.Flash_Valve.fvai.series.values);            %Example_3
plot(tout,simlog.Slurry_Valve3.Slurry_Valve.fvai.series.values); hold on;   %Example_4
plot(tout,simlog.Slurry_Valve4.Slurry_Valve.fvai.series.values);            %Example_4
xlabel('time (seconds)');
ylabel('Valve position (fraction open)');
ylim([-0.1,1.1])
title('Discharge Slurry Valve');
subplot(2,1,2)
plot(tout,simlog.Vent_Valve2.Vent_Valve.fvai.series.values);                %Example_2+4
% plot(tout,simlog.Modulating_Valve1.Modulating_Valve.fvai.series.values);  %Example_3
xlabel('time (seconds)');
ylabel('Valve position (fraction open)');
title('Vent Valve');
ylim([-0.1,1.1])

%% rate of external heat transfer
figure;
subplot(2,2,1)
plot(tout,(1e-3)*simlog.Autoclave_Pot_1.Pressure_Reactor.dQi.series.values,'Linewidth',1.5);
xlabel('time (seconds)'); ylabel('Rate of external heat transfer (kJ/s)');
xlim([0 200]); %ylim([-1, 100]);  
title('Pot1');

subplot(2,2,2)
plot(tout,(1e-3)*simlog.Autoclave_Pot_2.Pressure_Reactor.dQi.series.values,'Linewidth',1.5);
xlabel('time (seconds)'); ylabel('Rate of external heat transfer (kJ/s)');
xlim([0 200]); %ylim([-1, 100]);  
title('Pot2');

subplot(2,2,3)
plot(tout,(1e-3)*simlog.Autoclave_Pot_3.Pressure_Reactor.dQi.series.values,'Linewidth',1.5);
xlabel('time (seconds)'); ylabel('Rate of external heat transfer (kJ/s)');
xlim([0 200]);  %ylim([-1, 100]); 
title('Pot3');

subplot(2,2,4)
plot(tout,(1e-3)*simlog.Autoclave_Pot_4.Pressure_Reactor.dQi.series.values,'Linewidth',1.5);
xlabel('time (seconds)'); ylabel('Rate of external heat transfer (kJ/s)');
xlim([0 200]);  %ylim([-1, 100]); 
title('Pot4');


%% plot other species solution concentration
figure;
plot_solution_conc(1,tout,Conc_pot1.Data(:,5),Conc_pot1.Data(:,10),Conc_pot1.Data(:,3),Conc_pot1.Data(:,9),Conc_pot1.Data(:,11),'Pot1')
plot_solution_conc(2,tout,Conc_pot2.Data(:,5),Conc_pot2.Data(:,10),Conc_pot2.Data(:,3),Conc_pot2.Data(:,9),Conc_pot2.Data(:,11),'Pot2')
plot_solution_conc(3,tout,Conc_pot3.Data(:,5),Conc_pot3.Data(:,10),Conc_pot3.Data(:,3),Conc_pot3.Data(:,9),Conc_pot3.Data(:,11),'Pot3')
plot_solution_conc(4,tout,Conc_pot4.Data(:,5),Conc_pot4.Data(:,10),Conc_pot4.Data(:,3),Conc_pot4.Data(:,9),Conc_pot4.Data(:,11),'Pot4')


%% plot oxygen consumption
figure; plot(O2_cons_pot1.Time, O2_cons_pot1.Data,'b'); hold on;
plot(O2_cons_pot2.Time, O2_cons_pot2.Data,'g');
plot(O2_cons_pot3.Time, O2_cons_pot3.Data,'y');
plot(O2_cons_pot4.Time, O2_cons_pot4.Data,'m');
legend('pot1','pot2','pot3','pot4')
xlabel('time (seconds)'); ylabel('oxygen mass (kg)');