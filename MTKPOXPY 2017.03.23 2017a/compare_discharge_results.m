%% discharge investigtion
clear
clc

plantParameters_POXPY

%% example 1 pressure discharge
simlog_ex2 = simlog;
time_ex2 = tout;

O2_cons_pot1_ex2 = O2_cons_pot1.Data;
O2_cons_pot2_ex2 = O2_cons_pot2.Data;
O2_cons_pot3_ex2 = O2_cons_pot3.Data;
O2_cons_pot4_ex2 = O2_cons_pot4.Data;

solution_conc_pot1_ex2 = Conc_pot1;
solution_conc_pot2_ex2 = Conc_pot2;
solution_conc_pot3_ex2 = Conc_pot3;
solution_conc_pot4_ex2 = Conc_pot4;

extent_pot1_ex2 = Extents_A1.Data;
extent_pot2_ex2 = Extents_A2.Data;
extent_pot3_ex2 = Extents_A3.Data;
extent_pot4_ex2 = Extents_A4.Data;

%% example 3 flash

simlog_ex3 = simlog;
time_ex3 = tout;

O2_cons_pot1_ex3 = O2_cons_pot1.Data;
O2_cons_pot2_ex3 = O2_cons_pot2.Data;
O2_cons_pot3_ex3 = O2_cons_pot3.Data;
O2_cons_pot4_ex3 = O2_cons_pot4.Data;

solution_conc_pot1_ex3 = Conc_pot1;
solution_conc_pot2_ex3 = Conc_pot2;
solution_conc_pot3_ex3 = Conc_pot3;
solution_conc_pot4_ex3 = Conc_pot4;

extent_pot1_ex3 = Extents_A1.Data;
extent_pot2_ex3 = Extents_A2.Data;
extent_pot3_ex3 = Extents_A3.Data;
extent_pot4_ex3 = Extents_A4.Data;

%% example 4 timed

simlog_ex4 = simlog;
time_ex4 = tout;

O2_cons_pot1_ex4 = O2_cons_pot1.Data;
O2_cons_pot2_ex4 = O2_cons_pot2.Data;
O2_cons_pot3_ex4 = O2_cons_pot3.Data;
O2_cons_pot4_ex4 = O2_cons_pot4.Data;

solution_conc_pot1_ex4 = Conc_pot1;
solution_conc_pot2_ex4 = Conc_pot2;
solution_conc_pot3_ex4 = Conc_pot3;
solution_conc_pot4_ex4 = Conc_pot4;

extent_pot1_ex4 = Extents_A1.Data;
extent_pot2_ex4 = Extents_A2.Data;
extent_pot3_ex4 = Extents_A3.Data;
extent_pot4_ex4 = Extents_A4.Data;

%% plot level
figure;
L_SP = repmat(L_sl_oG_A4,1,length(tout));
plot(tout,L_SP,'k-','DisplayName','setpoint','Linewidth',1.5); hold on;
plot(time_ex2,simlog_ex2.Autoclave_Pot_4.Pressure_Reactor.L_meas.series.values,'b','DisplayName','Ex1 - pressure discharge','Linewidth',1.5);
plot(time_ex3,simlog_ex3.Autoclave_Pot_4.Pressure_Reactor.L_meas.series.values,'g','DisplayName','Ex2 - flash discharge','Linewidth',1.5);
plot(time_ex4,simlog_ex4.Autoclave_Pot_4.Pressure_Reactor.L_meas.series.values,'m','DisplayName','Ex3 - timed discharge','Linewidth',1.5);
xlabel('Time (seconds)'); ylabel('Level (m)')
legend show

%% plot pressure
figure;
P_SP = (1e-5)*repmat(Pg+P_atm,1,length(tout));
plot(time_ex2, (1e-5)*simlog_ex2.Autoclave_Pot_4.Pressure_Reactor.P.series.values,'b','DisplayName','Ex1 - pressure discharge','Linewidth',1.5); hold on
plot(time_ex3, (1e-5)*simlog_ex3.Autoclave_Pot_4.Pressure_Reactor.P.series.values,'g','DisplayName','Ex2 - flash discharge','Linewidth',1.5);
plot(time_ex4, (1e-5)*simlog_ex4.Autoclave_Pot_4.Pressure_Reactor.P.series.values,'m','DisplayName','Ex3 - timed discharge','Linewidth',1.5);
plot(tout, P_SP,'k-','DisplayName','setpoint','Linewidth',1.5); hold on;
xlabel('Time (seconds)'); ylabel('Pressure (bar)')
legend show

%% plot temperature
figure;
plot_Temp(1,time_ex2,simlog_ex2.Autoclave_Pot_1.Pressure_Reactor.T.series.values-273.15,'Pot1','b','Ex1 - pressure discharge');
plot_Temp(2,time_ex2,simlog_ex2.Autoclave_Pot_2.Pressure_Reactor.T.series.values-273.15,'Pot2','b','Ex1 - pressure discharge');
plot_Temp(3,time_ex2,simlog_ex2.Autoclave_Pot_3.Pressure_Reactor.T.series.values-273.15,'Pot3','b','Ex1 - pressure discharge');
plot_Temp(4,time_ex2,simlog_ex2.Autoclave_Pot_4.Pressure_Reactor.T.series.values-273.15,'Pot4','b','Ex1 - pressure discharge');
plot_Temp(1,time_ex3,simlog_ex3.Autoclave_Pot_1.Pressure_Reactor.T.series.values-273.15,'Pot1','g','Ex2 - flash discharge');
plot_Temp(2,time_ex3,simlog_ex3.Autoclave_Pot_2.Pressure_Reactor.T.series.values-273.15,'Pot2','g','Ex2 - flash discharge');
plot_Temp(3,time_ex3,simlog_ex3.Autoclave_Pot_3.Pressure_Reactor.T.series.values-273.15,'Pot3','g','Ex2 - flash discharge');
plot_Temp(4,time_ex3,simlog_ex3.Autoclave_Pot_4.Pressure_Reactor.T.series.values-273.15,'Pot4','g','Ex2 - flash discharge');
plot_Temp(1,time_ex4,simlog_ex4.Autoclave_Pot_1.Pressure_Reactor.T.series.values-273.15,'Pot1','m','Ex3 - timed discharge');
plot_Temp(2,time_ex4,simlog_ex4.Autoclave_Pot_2.Pressure_Reactor.T.series.values-273.15,'Pot2','m','Ex3 - timed discharge');
plot_Temp(3,time_ex4,simlog_ex4.Autoclave_Pot_3.Pressure_Reactor.T.series.values-273.15,'Pot3','m','Ex3 - timed discharge');
plot_Temp(4,time_ex4,simlog_ex4.Autoclave_Pot_4.Pressure_Reactor.T.series.values-273.15,'Pot4','m','Ex3 - timed discharge');
legend show;

%% pressure pots
figure;
plot_Pressure(1,time_ex2,(1e-5)*simlog_ex2.Autoclave_Pot_1.Pressure_Reactor.P.series.values,'Pot1','b','Ex1 - pressure discharge');
plot_Pressure(2,time_ex2,(1e-5)*simlog_ex2.Autoclave_Pot_2.Pressure_Reactor.P.series.values,'Pot2','b','Ex1 - pressure discharge');
plot_Pressure(3,time_ex2,(1e-5)*simlog_ex2.Autoclave_Pot_3.Pressure_Reactor.P.series.values,'Pot3','b','Ex1 - pressure discharge');
plot_Pressure(4,time_ex2,(1e-5)*simlog_ex2.Autoclave_Pot_4.Pressure_Reactor.P.series.values,'Pot4','b','Ex1 - pressure discharge');
plot_Pressure(1,time_ex4,(1e-5)*simlog_ex4.Autoclave_Pot_1.Pressure_Reactor.P.series.values,'Pot1','m','Ex3 - timed discharge');
plot_Pressure(2,time_ex4,(1e-5)*simlog_ex4.Autoclave_Pot_2.Pressure_Reactor.P.series.values,'Pot2','m','Ex3 - timed discharge');
plot_Pressure(3,time_ex4,(1e-5)*simlog_ex4.Autoclave_Pot_3.Pressure_Reactor.P.series.values,'Pot3','m','Ex3 - timed discharge');
plot_Pressure(4,time_ex4,(1e-5)*simlog_ex4.Autoclave_Pot_4.Pressure_Reactor.P.series.values,'Pot4','m','Ex3 - timed discharge');
plot_Pressure(1,time_ex3,(1e-5)*simlog_ex3.Autoclave_Pot_1.Pressure_Reactor.P.series.values,'Pot1','g','Ex2 - flash discharge');
plot_Pressure(2,time_ex3,(1e-5)*simlog_ex3.Autoclave_Pot_2.Pressure_Reactor.P.series.values,'Pot2','g','Ex2 - flash discharge');
plot_Pressure(3,time_ex3,(1e-5)*simlog_ex3.Autoclave_Pot_3.Pressure_Reactor.P.series.values,'Pot3','g','Ex2 - flash discharge');
plot_Pressure(4,time_ex3,(1e-5)*simlog_ex3.Autoclave_Pot_4.Pressure_Reactor.P.series.values,'Pot4','g','Ex2 - flash discharge');
legend show;

%% oxygen consumption
figure;
plot_O2_consumption(1,time_ex2,O2_cons_pot1_ex2,'Pot1','b','Ex1 - pressure discharge');
plot_O2_consumption(2,time_ex2,O2_cons_pot1_ex2,'Pot2','b','Ex1 - pressure discharge');
plot_O2_consumption(3,time_ex2,O2_cons_pot1_ex2,'Pot3','b','Ex1 - pressure discharge');
plot_O2_consumption(4,time_ex2,O2_cons_pot1_ex2,'Pot4','b','Ex1 - pressure discharge');
plot_O2_consumption(1,time_ex3,O2_cons_pot1_ex3,'Pot1','g','Ex2 - flash discharge');
plot_O2_consumption(2,time_ex3,O2_cons_pot1_ex3,'Pot2','g','Ex2 - flash discharge');
plot_O2_consumption(3,time_ex3,O2_cons_pot1_ex3,'Pot3','g','Ex2 - flash discharge');
plot_O2_consumption(4,time_ex3,O2_cons_pot1_ex3,'Pot4','g','Ex2 - flash discharge');
plot_O2_consumption(1,time_ex4,O2_cons_pot1_ex4,'Pot1','m','Ex3 - timed discharge');
plot_O2_consumption(2,time_ex4,O2_cons_pot1_ex4,'Pot2','m','Ex3 - timed discharge');
plot_O2_consumption(3,time_ex4,O2_cons_pot1_ex4,'Pot3','m','Ex3 - timed discharge');
plot_O2_consumption(4,time_ex4,O2_cons_pot1_ex4,'Pot4','m','Ex3 - timed discharge');
legend show;

%% solution concentration
figure;
plot_solution_O2(1,time_ex2,solution_conc_pot1_ex2.Data(:,3),'Pot1','b','Ex1 - pressure discharge')
plot_solution_O2(2,time_ex2,solution_conc_pot2_ex2.Data(:,3),'Pot2','b','Ex1 - pressure discharge')
plot_solution_O2(3,time_ex2,solution_conc_pot3_ex2.Data(:,3),'Pot3','b','Ex1 - pressure discharge')
plot_solution_O2(4,time_ex2,solution_conc_pot4_ex2.Data(:,3),'Pot4','b','Ex1 - pressure discharge')
plot_solution_O2(1,time_ex3,solution_conc_pot1_ex3.Data(:,3),'Pot1','g','Ex2 - flash discharge')
plot_solution_O2(2,time_ex3,solution_conc_pot2_ex3.Data(:,3),'Pot2','g','Ex2 - flash discharge')
plot_solution_O2(3,time_ex3,solution_conc_pot3_ex3.Data(:,3),'Pot3','g','Ex2 - flash discharge')
plot_solution_O2(4,time_ex3,solution_conc_pot4_ex3.Data(:,3),'Pot4','g','Ex2 - flash discharge')
plot_solution_O2(1,time_ex4,solution_conc_pot1_ex4.Data(:,3),'Pot1','m','Ex3 - timed discharge')
plot_solution_O2(2,time_ex4,solution_conc_pot2_ex4.Data(:,3),'Pot2','m','Ex3 - timed discharge')
plot_solution_O2(3,time_ex4,solution_conc_pot3_ex4.Data(:,3),'Pot3','m','Ex3 - timed discharge')
plot_solution_O2(4,time_ex4,solution_conc_pot4_ex4.Data(:,3),'Pot4','m','Ex3 - timed discharge')
legend show;

figure;
plot_solution_Fe2(1,time_ex2,solution_conc_pot1_ex2.Data(:,9),'Pot1','b','Ex1 - pressure discharge')
plot_solution_Fe2(2,time_ex2,solution_conc_pot2_ex2.Data(:,9),'Pot2','b','Ex1 - pressure discharge')
plot_solution_Fe2(3,time_ex2,solution_conc_pot3_ex2.Data(:,9),'Pot3','b','Ex1 - pressure discharge')
plot_solution_Fe2(4,time_ex2,solution_conc_pot4_ex2.Data(:,9),'Pot4','b','Ex1 - pressure discharge')
plot_solution_Fe2(1,time_ex3,solution_conc_pot1_ex3.Data(:,9),'Pot1','g','Ex2 - flash discharge')
plot_solution_Fe2(2,time_ex3,solution_conc_pot2_ex3.Data(:,9),'Pot2','g','Ex2 - flash discharge')
plot_solution_Fe2(3,time_ex3,solution_conc_pot3_ex3.Data(:,9),'Pot3','g','Ex2 - flash discharge')
plot_solution_Fe2(4,time_ex3,solution_conc_pot4_ex3.Data(:,9),'Pot4','g','Ex2 - flash discharge')
plot_solution_Fe2(1,time_ex4,solution_conc_pot1_ex4.Data(:,9),'Pot1','m','Ex3 - timed discharge')
plot_solution_Fe2(2,time_ex4,solution_conc_pot2_ex4.Data(:,9),'Pot2','m','Ex3 - timed discharge')
plot_solution_Fe2(3,time_ex4,solution_conc_pot3_ex4.Data(:,9),'Pot3','m','Ex3 - timed discharge')
plot_solution_Fe2(4,time_ex4,solution_conc_pot4_ex4.Data(:,9),'Pot4','m','Ex3 - timed discharge')
legend show;

figure;
plot_solution_Fe3(1,time_ex2,solution_conc_pot1_ex2.Data(:,10),'Pot1','b','Ex1 - pressure discharge')
plot_solution_Fe3(2,time_ex2,solution_conc_pot2_ex2.Data(:,10),'Pot2','b','Ex1 - pressure discharge')
plot_solution_Fe3(3,time_ex2,solution_conc_pot3_ex2.Data(:,10),'Pot3','b','Ex1 - pressure discharge')
plot_solution_Fe3(4,time_ex2,solution_conc_pot4_ex2.Data(:,10),'Pot4','b','Ex1 - pressure discharge')
plot_solution_Fe3(1,time_ex3,solution_conc_pot1_ex3.Data(:,10),'Pot1','g','Ex2 - flash discharge')
plot_solution_Fe3(2,time_ex3,solution_conc_pot2_ex3.Data(:,10),'Pot2','g','Ex2 - flash discharge')
plot_solution_Fe3(3,time_ex3,solution_conc_pot3_ex3.Data(:,10),'Pot3','g','Ex2 - flash discharge')
plot_solution_Fe3(4,time_ex3,solution_conc_pot4_ex3.Data(:,10),'Pot4','g','Ex2 - flash discharge')
plot_solution_Fe3(1,time_ex4,solution_conc_pot1_ex4.Data(:,10),'Pot1','m','Ex3 - timed discharge')
plot_solution_Fe3(2,time_ex4,solution_conc_pot2_ex4.Data(:,10),'Pot2','m','Ex3 - timed discharge')
plot_solution_Fe3(3,time_ex4,solution_conc_pot3_ex4.Data(:,10),'Pot3','m','Ex3 - timed discharge')
plot_solution_Fe3(4,time_ex4,solution_conc_pot4_ex4.Data(:,10),'Pot4','m','Ex3 - timed discharge')
legend show;

figure;
plot_solution_acid(1,time_ex2,solution_conc_pot1_ex2.Data(:,5),'Pot1','b','Ex1 - pressure discharge')
plot_solution_acid(2,time_ex2,solution_conc_pot2_ex2.Data(:,5),'Pot2','b','Ex1 - pressure discharge')
plot_solution_acid(3,time_ex2,solution_conc_pot3_ex2.Data(:,5),'Pot3','b','Ex1 - pressure discharge')
plot_solution_acid(4,time_ex2,solution_conc_pot4_ex2.Data(:,5),'Pot4','b','Ex1 - pressure discharge')
plot_solution_acid(1,time_ex3,solution_conc_pot1_ex3.Data(:,5),'Pot1','g','Ex2 - flash discharge')
plot_solution_acid(2,time_ex3,solution_conc_pot2_ex3.Data(:,5),'Pot2','g','Ex2 - flash discharge')
plot_solution_acid(3,time_ex3,solution_conc_pot3_ex3.Data(:,5),'Pot3','g','Ex2 - flash discharge')
plot_solution_acid(4,time_ex3,solution_conc_pot4_ex3.Data(:,5),'Pot4','g','Ex2 - flash discharge')
plot_solution_acid(1,time_ex4,solution_conc_pot1_ex4.Data(:,5),'Pot1','m','Ex3 - timed discharge')
plot_solution_acid(2,time_ex4,solution_conc_pot2_ex4.Data(:,5),'Pot2','m','Ex3 - timed discharge')
plot_solution_acid(3,time_ex4,solution_conc_pot3_ex4.Data(:,5),'Pot3','m','Ex3 - timed discharge')
plot_solution_acid(4,time_ex4,solution_conc_pot4_ex4.Data(:,5),'Pot4','m','Ex3 - timed discharge')
legend show;

%% reaction extents
figure;
plot_extents(1,time_ex2,extent_pot1_ex2,'Pot1','b','Ex1 - pressure discharge');
plot_extents(2,time_ex2,extent_pot2_ex2,'Pot2','b','Ex1 - pressure discharge');
plot_extents(3,time_ex2,extent_pot3_ex2,'Pot3','b','Ex1 - pressure discharge');
plot_extents(4,time_ex2,extent_pot4_ex2,'Pot4','b','Ex1 - pressure discharge');
plot_extents(1,time_ex3,extent_pot1_ex3,'Pot1','g','Ex2 - flash discharge');
plot_extents(2,time_ex3,extent_pot2_ex3,'Pot2','g','Ex2 - flash discharge');
plot_extents(3,time_ex3,extent_pot3_ex3,'Pot3','g','Ex2 - flash discharge');
plot_extents(4,time_ex3,extent_pot4_ex3,'Pot4','g','Ex2 - flash discharge');
plot_extents(1,time_ex4,extent_pot1_ex4,'Pot1','m','Ex3 - timed discharge');
plot_extents(2,time_ex4,extent_pot2_ex4,'Pot2','m','Ex3 - timed discharge');
plot_extents(3,time_ex4,extent_pot3_ex4,'Pot3','m','Ex3 - timed discharge');
plot_extents(4,time_ex4,extent_pot4_ex4,'Pot4','m','Ex3 - timed discharge');
legend show;

%% vent valve movements
figure;
subplot(3,1,1);
plot(time_ex2,simlog_ex2.Modulating_Valve1.Modulating_Valve.fvai.series.values);
subplot(3,1,2);
plot(time_ex3,simlog_ex3.Modulating_Valve1.Modulating_Valve.fvai.series.values);
subplot(3,1,3);
plot(time_ex4,simlog_ex4.Vent_Valve4.Vent_Valve.fvai.series.values);

%% slurry valve movements
figure;
subplot(3,1,1);
plot(time_ex2,simlog_ex2.Slurry_Valve2.Slurry_Valve.fvai.series.values);
xlabel('time (seconds)'); ylabel('fraction of valve position')
title('Ex1 - pressure discharge')
subplot(3,1,2);
plot(time_ex3,simlog_ex3.Flash_Valve1.Flash_Valve.fvai.series.values);
xlabel('time (seconds)'); ylabel('fraction of valve position')
title('Ex2 - flash discharge')
subplot(3,1,3);
plot(time_ex4,simlog_ex4.Slurry_Valve3.Slurry_Valve.fvai.series.values);
xlabel('time (seconds)'); ylabel('fraction of valve position')
title('Ex3 - timed discharge')
% plot(time_ex4,simlog_ex4.Slurry_Valve4.Slurry_Valve.fvai.series.values);

figure; plot(time_ex4,simlog_ex4.Slurry_Valve3.Slurry_Valve.fvai.series.values);
xlabel('time (seconds)'); ylabel('fraction of valve position')
% title('Ex3 - timed discharge')


%% energy consumption
figure;
subplot(2,2,1);
plot(time_ex4,(1e-3)*simlog_ex4.Autoclave_Pot_1.Pressure_Reactor.dQi.series.values,'m');hold on;
plot(time_ex2,(1e-3)*simlog_ex2.Autoclave_Pot_1.Pressure_Reactor.dQi.series.values,'b'); 
plot(time_ex3,(1e-3)*simlog_ex3.Autoclave_Pot_1.Pressure_Reactor.dQi.series.values,'g');
xlabel('time (seconds)'); ylabel('Energy (kJ/s)');
ylim([-10 30])
title('Pot1')

subplot(2,2,2);
plot(time_ex4,(1e-3)*simlog_ex4.Autoclave_Pot_2.Pressure_Reactor.dQi.series.values,'m'); hold on;
plot(time_ex2,(1e-3)*simlog_ex2.Autoclave_Pot_2.Pressure_Reactor.dQi.series.values,'b'); 
plot(time_ex3,(1e-3)*simlog_ex3.Autoclave_Pot_2.Pressure_Reactor.dQi.series.values,'g');
xlabel('time (seconds)'); ylabel('Energy (kJ/s)');
ylim([-10 30])
title('Pot2')

subplot(2,2,3);
plot(time_ex4,(1e-3)*simlog_ex4.Autoclave_Pot_3.Pressure_Reactor.dQi.series.values,'m'); hold on;
plot(time_ex2,(1e-3)*simlog_ex2.Autoclave_Pot_3.Pressure_Reactor.dQi.series.values,'b'); 
plot(time_ex3,(1e-3)*simlog_ex3.Autoclave_Pot_3.Pressure_Reactor.dQi.series.values,'g');

xlabel('time (seconds)'); ylabel('Energy (kJ/s)');
ylim([-10 30])
title('Pot3')

subplot(2,2,4);
plot(time_ex4,(1e-3)*simlog_ex4.Autoclave_Pot_4.Pressure_Reactor.dQi.series.values,'m'); hold on;
plot(time_ex2,(1e-3)*simlog_ex2.Autoclave_Pot_4.Pressure_Reactor.dQi.series.values,'b'); 
plot(time_ex3,(1e-3)*simlog_ex3.Autoclave_Pot_4.Pressure_Reactor.dQi.series.values,'g');
xlabel('time (seconds)'); ylabel('Energy (kJ/s)');
ylim([-10 30])
title('Pot4')
legend('Ex1 - pressure discharge','Ex2 - flash discharge','Ex3 - timed discharge')
legend show

%% calculate cumulative energy consumption
energy_P1_ex2 = simlog_ex2.Autoclave_Pot_1.Pressure_Reactor.dQi.series.values;
energy_P1_ex3 = simlog_ex3.Autoclave_Pot_1.Pressure_Reactor.dQi.series.values;
energy_P1_ex4 = simlog_ex4.Autoclave_Pot_1.Pressure_Reactor.dQi.series.values;

energy_P2_ex2 = simlog_ex2.Autoclave_Pot_2.Pressure_Reactor.dQi.series.values;
energy_P2_ex3 = simlog_ex3.Autoclave_Pot_2.Pressure_Reactor.dQi.series.values;
energy_P2_ex4 = simlog_ex4.Autoclave_Pot_2.Pressure_Reactor.dQi.series.values;

energy_P3_ex2 = simlog_ex2.Autoclave_Pot_1.Pressure_Reactor.dQi.series.values;
energy_P3_ex3 = simlog_ex3.Autoclave_Pot_1.Pressure_Reactor.dQi.series.values;
energy_P3_ex4 = simlog_ex4.Autoclave_Pot_1.Pressure_Reactor.dQi.series.values;

energy_P4_ex2 = simlog_ex2.Autoclave_Pot_1.Pressure_Reactor.dQi.series.values;
energy_P4_ex3 = simlog_ex3.Autoclave_Pot_1.Pressure_Reactor.dQi.series.values;
energy_P4_ex4 = simlog_ex4.Autoclave_Pot_1.Pressure_Reactor.dQi.series.values;


cumulative_heating_P1_ex2 = (1e-6)*trapz(time_ex2(energy_P1_ex2 > 0),energy_P1_ex2(energy_P1_ex2 > 0));
cumulative_cooling_P1_ex2 = (1e-6)*trapz(time_ex2(energy_P1_ex2 < 0),energy_P1_ex2(energy_P1_ex2 < 0));
cumulative_heating_P2_ex2 = (1e-6)*trapz(time_ex2(energy_P2_ex2 > 0),energy_P2_ex2(energy_P2_ex2 > 0));
cumulative_cooling_P2_ex2 = (1e-6)*trapz(time_ex2(energy_P2_ex2 < 0),energy_P2_ex2(energy_P2_ex2 < 0));
cumulative_heating_P3_ex2 = (1e-6)*trapz(time_ex2(energy_P3_ex2 > 0),energy_P3_ex2(energy_P3_ex2 > 0));
cumulative_cooling_P3_ex2 = (1e-6)*trapz(time_ex2(energy_P3_ex2 < 0),energy_P3_ex2(energy_P3_ex2 < 0));
cumulative_heating_P4_ex2 = (1e-6)*trapz(time_ex2(energy_P4_ex2 > 0),energy_P4_ex2(energy_P4_ex2 > 0));
cumulative_cooling_P4_ex2 = (1e-6)*trapz(time_ex2(energy_P4_ex2 < 0),energy_P4_ex2(energy_P4_ex2 < 0));

cumulative_heating_P1_ex3 = (1e-6)*trapz(time_ex3(energy_P1_ex3 > 0),energy_P1_ex3(energy_P1_ex3 > 0));
cumulative_cooling_P1_ex3 = (1e-6)*trapz(time_ex3(energy_P1_ex3 < 0),energy_P1_ex3(energy_P1_ex3 < 0));
cumulative_heating_P2_ex3 = (1e-6)*trapz(time_ex3(energy_P2_ex3 > 0),energy_P2_ex3(energy_P2_ex3 > 0));
cumulative_cooling_P2_ex3 = (1e-6)*trapz(time_ex3(energy_P2_ex3 < 0),energy_P2_ex3(energy_P2_ex3 < 0));
cumulative_heating_P3_ex3 = (1e-6)*trapz(time_ex3(energy_P3_ex3 > 0),energy_P3_ex3(energy_P3_ex3 > 0));
cumulative_cooling_P3_ex3 = (1e-6)*trapz(time_ex3(energy_P3_ex3 < 0),energy_P3_ex3(energy_P3_ex3 < 0));
cumulative_heating_P4_ex3 = (1e-6)*trapz(time_ex3(energy_P4_ex3 > 0),energy_P4_ex3(energy_P4_ex3 > 0));
cumulative_cooling_P4_ex3 = (1e-6)*trapz(time_ex3(energy_P4_ex3 < 0),energy_P4_ex3(energy_P4_ex3 < 0));

cumulative_heating_P1_ex4 = (1e-6)*trapz(time_ex4(energy_P1_ex4 > 0),energy_P1_ex4(energy_P1_ex4 > 0));
cumulative_cooling_P1_ex4 = (1e-6)*trapz(time_ex4(energy_P1_ex4 < 0),energy_P1_ex4(energy_P1_ex4 < 0));
cumulative_heating_P2_ex4 = (1e-6)*trapz(time_ex4(energy_P2_ex4 > 0),energy_P2_ex4(energy_P2_ex4 > 0));
cumulative_cooling_P2_ex4 = (1e-6)*trapz(time_ex4(energy_P2_ex4 < 0),energy_P2_ex4(energy_P2_ex4 < 0));
cumulative_heating_P3_ex4 = (1e-6)*trapz(time_ex4(energy_P3_ex4 > 0),energy_P3_ex4(energy_P3_ex4 > 0));
cumulative_cooling_P3_ex4 = (1e-6)*trapz(time_ex4(energy_P3_ex4 < 0),energy_P3_ex4(energy_P3_ex4 < 0));
cumulative_heating_P4_ex4 = (1e-6)*trapz(time_ex4(energy_P4_ex4 > 0),energy_P4_ex4(energy_P4_ex4 > 0));
cumulative_cooling_P4_ex4 = (1e-6)*trapz(time_ex4(energy_P4_ex4 < 0),energy_P4_ex4(energy_P4_ex4 < 0));

figure;
c = categorical({'Ex1 - pressure discharge','Ex2 - flash discharge','Ex3 - timed discharge'});
subplot(2,2,1);
bar(c,[cumulative_heating_P1_ex2,abs(cumulative_cooling_P1_ex2); cumulative_heating_P1_ex3,abs(cumulative_cooling_P1_ex3); cumulative_heating_P1_ex4, abs(cumulative_cooling_P1_ex4)])
ylim([0 60]);ylabel('Cumulative Energy (MJ)');title('Pot1')
subplot(2,2,2);
bar(c,[cumulative_heating_P2_ex2,abs(cumulative_cooling_P2_ex2); cumulative_heating_P2_ex3,abs(cumulative_cooling_P2_ex3); cumulative_heating_P2_ex4, abs(cumulative_cooling_P2_ex4)])
ylim([0 60]);ylabel('Cumulative Energy (MJ)');title('Pot2')
subplot(2,2,3)
bar(c,[cumulative_heating_P3_ex2,abs(cumulative_cooling_P3_ex2); cumulative_heating_P3_ex3,abs(cumulative_cooling_P3_ex3); cumulative_heating_P3_ex4, abs(cumulative_cooling_P3_ex4)])
ylim([0 60]);ylabel('Cumulative Energy (MJ)');title('Pot3')
subplot(2,2,4)
bar(c,[cumulative_heating_P4_ex2,abs(cumulative_cooling_P4_ex2); cumulative_heating_P4_ex3,abs(cumulative_cooling_P4_ex3); cumulative_heating_P4_ex4, abs(cumulative_cooling_P4_ex4)])
ylim([0 60]);ylabel('Cumulative Energy (MJ)');title('Pot4')
legend('heating','cooling');
legend show

cumulative_heating_ex2 = cumulative_heating_P1_ex2 + cumulative_heating_P2_ex2 + cumulative_heating_P3_ex2 + cumulative_heating_P4_ex2;
cumulative_heating_ex3 = cumulative_heating_P1_ex3 + cumulative_heating_P2_ex3 + cumulative_heating_P3_ex3 + cumulative_heating_P4_ex3;
cumulative_heating_ex4 = cumulative_heating_P1_ex4 + cumulative_heating_P2_ex4 + cumulative_heating_P3_ex4 + cumulative_heating_P4_ex4;

cumulative_cooling_ex2 = cumulative_cooling_P1_ex2 + cumulative_cooling_P2_ex2 + cumulative_cooling_P3_ex2 + cumulative_cooling_P4_ex2;
cumulative_cooling_ex3 = cumulative_cooling_P1_ex3 + cumulative_cooling_P2_ex3 + cumulative_cooling_P3_ex3 + cumulative_cooling_P4_ex3;
cumulative_cooling_ex4 = cumulative_cooling_P1_ex4 + cumulative_cooling_P2_ex4 + cumulative_cooling_P3_ex4 + cumulative_cooling_P4_ex4;

%% calculate extent of reaction
dtFeed_mass_ex2 = reshape(simlog_ex2.CTflow_Sensor2.Flow_Sensor.dm.series.values,[],length(time_ex2));
Feed_FeS_ex2 = trapz(time_ex2,dtFeed_mass_ex2(8,:));
dtDischarge_mass_ex2 = reshape(simlog_ex2.Tflow_Sensor9.Flow_Sensor.dm.series.values,[],length(time_ex2));
Discharge_FeS_ex2 = trapz(time_ex2,dtDischarge_mass_ex2(8,:));
conversion_ex2 = 100*(Feed_FeS_ex2-Discharge_FeS_ex2)/Feed_FeS_ex2;

dtFeed_mass_ex3 = reshape(simlog_ex3.CTflow_Sensor2.Flow_Sensor.dm.series.values,[],length(time_ex3));
Feed_FeS_ex3 = trapz(time_ex3,dtFeed_mass_ex3(8,:));
dtDischarge_mass_ex3 = reshape(simlog_ex3.CTflow_Sensor3.Flow_Sensor.dm.series.values,[],length(time_ex3));
Discharge_FeS_ex3 = trapz(time_ex3,dtDischarge_mass_ex3(8,:));
conversion_ex3 = 100*(Feed_FeS_ex3-Discharge_FeS_ex3)/Feed_FeS_ex3;

dtFeed_mass_ex4 = reshape(simlog_ex4.CTflow_Sensor2.Flow_Sensor.dm.series.values,[],length(time_ex4));
Feed_FeS_ex4 = trapz(time_ex4,dtFeed_mass_ex4(8,:));
dtDischarge_mass_ex4 = reshape(simlog_ex4.CTflow_Sensor3.Flow_Sensor.dm.series.values,[],length(time_ex4));
Discharge_FeS_ex4 = trapz(time_ex4,dtDischarge_mass_ex4(8,:));
conversion_ex4 = 100*(Feed_FeS_ex4-Discharge_FeS_ex4)/Feed_FeS_ex4;

%% plot extent

c = categorical({'Ex1 - pressure discharge','Ex2 - flash discharge','Ex3 - timed discharge'});
figure; hbar = bar(c,[conversion_ex2, conversion_ex3, conversion_ex4]);
ylabel('% Conversion')

% Get the data for all the bars that were plotted
x = get(hbar,'XData');
y = get(hbar,'YData');

ygap = 0.1;  % Specify vertical gap between the bar and label
ylimits = get(gca,'YLim');
set(gca,'YLim',[ylimits(1),ylimits(2)+0.2*max(y)]); % Increase y limit for labels

% Create labels to place over bars
labels = {num2str(round(conversion_ex2,1)), num2str(round(conversion_ex3,1)),num2str(round(conversion_ex4,1))}; 

for i = 1:length(x) % Loop over each bar 
    xpos = x(i);        % Set x position for the text label
    ypos = y(i) + ygap; % Set y position, including gap
    htext = text(xpos,ypos,labels{i});          % Add text label
    set(htext,'VerticalAlignment','bottom','HorizontalAlignment','center')% Adjust properties
end

 
