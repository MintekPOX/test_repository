%% This script file sets the user-defined constants and parameters for the Mintek Batch & Pilot Plant simulations

%% Temperature Controller parameters
Kc = 1e+4;
Ti = 0.1;

%% Atmospheric conditions
P_atm = 8.8e4; % Atmospheric pressure (Pa) (abs)
T_atm = 20+273.15; % Atmospheric temperature (K)

total_time = 10000;
step_size = 5;

%% Flowrate setpoint (into circuit)
dmt_i = 15/60/60; % kg/s
td_period = 120; % Discharge period (s)
td_width = 1; % Discharge pulse width (%)
td_delay = 30; % Valve delay (s)


%% Species properties
rho = [2600;2070;5470;5450;4760;5600;4200;5020;4740;5240;3180;2570;...
        1000;1000;1;1000;1;1;1;1;1;1;1;1;1;1]; % Density vector (kg/m^3)

MK =  [-14994749.2993926000 -1016.63229828757000 3.147571495382320000 -28420602.3869796000 -0.00164711918643195000000;...
       -183876.027269261000 523.4560199625700000 0.312819713038054000 0.000000000000000000 0.000000000000000000000000;...    
       -1135454.39564081000 494.1302474618030000 0.102233084380310000 3231834.840347580000 0.000001505912358839080000;...
       -1233883.09193382000 502.8258155554480000 0.078274750201113900 1120.962885138670000 0.000000005604814425693350;...
       -738659.039306789000 456.7795218515930000 0.105306867852851000 2198599.883062100000 -0.00000001743260294213530;...
       -136273.503420908000 65.46968950585260000 0.066355356276359200 0.000000000000000000 0.000000000000000000000000;...
       -898367.939655526000 414.8830702734160000 0.146243113795127000 1125506.532740420000 0.000000000000000000000000;...
       -1644689.92523477000 603.3909325064390000 0.036885143414439000 9525952.970400190000 0.000000002778541876793900;...
       -313734.537047214000 -3108.62616173911000 4.431854118557110000 -92416986.1330724000 0.000000000000000000000000;...
       -5481587.03467867000 899.0169839228220000 -0.11372815954692800 19683491.11603450000 0.000149854950126973000000;...
       -7072825.27518800000 828.7315768166000000 0.285013385178886000 13277430.44033690000 -0.00003121744790703090000;...         
       -11825620.1611607000 233.7221234589310000 1.362738296550030000 0.000000000000000000 0.000000000000000000000000;...
       -3129812.87588222000 8306.519026824700000 -7.92284778540237000 395888958.2033690000 0.003144655853607820000000;...
       -8600278.09794875000 1568.904953937610000 0.194762091696534000 23186691.61394260000 -0.00002937592723824480000;...
       -2501337.81763691000 7161.576684125650000 0.000000000000000000 0.000000000000000000 0.000000000000000000000000;...
       -18446777.9878350000 10373.62686161210000 -12.8847979217665000 108602132.6317080000 0.010151204809245500000000;...
       -9239683.21057791000 0.000000000000000000 2.159655768377800000 0.000000000000000000 0.000000000000000000000000;...
       -6106849.29945494000 0.000000000000000000 -1.32513125407913000 0.000000000000000000 0.000000000000000000000000;...
       -6116990.00402431000 0.000000000000000000 -1.40755555942675000 0.000000000000000000 0.000000000000000000000000;...
       -5163571.01568391000 0.000000000000000000 -1.43602380643581000 0.000000000000000000 0.000000000000000000000000;...
       -6456585.93065022000 0.000000000000000000 -1.30226471087775000 0.000000000000000000 0.000000000000000000000000;...
       -6911073.01575646000 0.000000000000000000 -1.72458678740020000 0.000000000000000000 0.000000000000000000000000;...
       -11287254.5895903000 0.000000000000000000 -1.64079071007879000 0.000000000000000000 0.000000000000000000000000;...
       -13900581.3160032000 1576.900190393720000 0.346276498309770000 -7125054.81451877000 0.000006657859338081150000;...
       -215300.401854190000 689.3867895046060000 0.326367238771454000 -5064439.91649687000 -0.00008549622694184360000;...
       -309890.494794418000 1042.072722340020000 -0.02000828175087640 0.000000000000000000 0.000036791916249604400000]; % Maier-Kelly coefficients
         
M = [0.0600843;0.0320600;0.0907534;0.0909932;0.0956063;0.7322167;...
      0.5018185;0.1199670;0.0879070;0.1596922;0.16891195;0.1383780;...
      0.0320600;0.0980735;0.0319988;0.0180153;0.0980735;0.1547510;...
      0.1549908;0.1596039;0.1519046;0.3998668;0.1203627;0.0180153;0.0319988;0.0280134]; % Molar mass (kg/mol)
        
  
%% Particulate ordinate (x1) values  
particleSize = [3.953E-01;6.602E-01;1.102E+00;1.841E+00;3.075E+00;5.135E+00;8.575E+00;...
    1.432E+01;2.391E+01;3.994E+01;6.669E+01;1.114E+02;1.860E+02;3.106E+02;5.187E+02]*1e-6; % Particle size (m)
N_x1 = 15; % Number of nodes
x1 = linspace(log(min(particleSize)),log(max(particleSize)),N_x1)'; % Log scale
phiv = pi/6; % Particle volume shape factor 


%% Reaction rate constants
% Reaction 2: So(s) <=> So(l)
% Elemental sulfur phase change: Rate (mol/s) = k_r2*nr*{f((T-Tm)/Tr)}
k_r2 = 0; % Rate constant (1/s)

% Reaction 3: 
kr_r3 = 0; % Rate constant (1/s) @ Tr
Tr_r3 = 145+273.14; % Reference temperature (K)
Ea_r3 = 55e3; % Activation energy (J/mol)

% Reaction 4: 
kr_r4 = 0; % Rate constant (1/s) @ Tr
Tr_r4 = 145+273.14; % Reference temperature (K)
Ea_r4 = 55e3; % Activation energy (J/mol)

% Reaction 5: 
kr_r5 = 0; % Rate constant (1/s) @ Tr
Tr_r5 = 145+273.14; % Reference temperature (K)
Ea_r5 = 55e3; % Activation energy (J/mol)

% Reaction 6: 
kr_r6 = 0; % Rate constant (1/s) @ Tr
Tr_r6 = 145+273.14; % Reference temperature (K)
Ea_r6 = 55e3; % Activation energy (J/mol)

% Reaction 7: 
kr_r7 = 0; % Rate constant (1/s) @ Tr
Tr_r7 = 145+273.14; % Reference temperature (K)
Ea_r7 = 75e3; % Activation energy (J/mol)

% Reaction 8: FeS2(s) + 7/2O2(a) + H2O(l) => FeSO4(a) + H2SO4(a)
% Pyrite leaching (shrinkage): Rate (m/s) = klr_r8*{exp(-Ea/Rg*(1/T-1/Tr))*[O2]^(1/2)*f([FeIII])} 
klr_r8 = -1e-6/60; % Linear leaching (shrinkage) rate (m/s) @ Tr
Tr_r8 = 145+273.14; % Reference temperature (K)
Ea_r8 = 75e3; % Activation energy (J/mol)

% Reaction 9: 
kr_r9 = 0; % Rate constant (1/s) @ Tr
Tr_r9 = 145+273.14; % Reference temperature (K)
Ea_r9 = 55e3; % Activation energy (J/mol)

% Reaction 14: H2SO4(l) <=> H2SO4(a)
% Sulfuric acid dilution: Rate (mol/s) = k_r14*nr*{f([HHSO4],[HHSO4]r)}
k_r14 = 1e+6; % Rate constant (1/s)

% Reaction 16: H2O(l) <=> H2O(g)
% Water evaporation: Rate (mol/s) = k_r16*nr*{f((pH2O*-pH2O)/Pr))}
k_r16 = 1e-6; % Rate constant (1/s)

% Reaction 20: MgSO4(a) + H2O(l) => MgSO4.H2O(s)
% Magnesium sulfate monohydrate (kieserite) precipitation: Rate (mol/s) = k_r20*nr*{f([MgSO4.nH2O],[Mg],[Mg*])}*m_H2O
k_r20 = 0; % (1/s)

% Reaction 21: FeSO4(a) + 1/4O2(a) + 1/2H2SO4(a) => 1/2Fe2(SO4)3(a) + 1/2H2O(l)
% Ferrous sulfate oxidation: Rate (mol/s) = k_r21*[FeII]*{Fe(II)*exp((-Ea/Rg)*(1/T - 1/Tr))*[O2]^1*[H2SO4]^-0.35}*m_H2O
kr_r21 = 1.5e+2; % Rate constant (1/s) at Tr
Tr_r21 = 145+273.14; % Reference temperature (K)
Ea_r21 = 55e3; % Activation energy (J/mol)

% Reaction 22: Fe2(SO4)3(a) + 3H2O(l) <=> Fe2O3(s) + 3H2SO4(a)
% Hematite precipitation: Rate (mol/s) = k_r22*nr*{f([Fe2(SO4)3],[H2SO4])}*m_H2O
k_r22 = 1e-2; % Rate constant (1/s)
% Reaction 23: Fe2(SO4)3(a) + 2H2O(l) <=> 2Fe(OH)SO4(s) + H2SO4(a)
% BFS precipitation: Rate (mol/s) = k_r23*nr*{f([Fe2(SO4)3],[H2SO4])}*m_H2O
k_r23 = 2e-2; % Rate constant (1/s)

% Reaction 25: O2(g) => O2(a)
% Oxygen gas dissolution: Rate (mol/s) = k_r25*([O2]*-[O2])*m_H2O
kr_r25 = 1.0/60; % Rate constant: kLa (1/s) @ 25C


%% Autoclave properties
% Operating parameters
Pg = 2.2e6; % Gauge pressure (Pa)
T = 200 + 273.15; % Temperature (K)

% Particulate phase
volFraction = [2.364E-04;7.413E-03;1.378E-02;2.292E-02;4.490E-02;5.307E-02;...
    1.044E-01;1.127E-01;2.078E-01;2.362E-01;1.517E-01;3.492E-02;2.922E-03;5.943E-03;1.0956E-03]; % Histogram mass fractions
fDm_user = volFraction; % User defined mass (volume) fraction distribution
fDm_init = fDm_user; % Initial mass (volume) fraction distribution

% Reactions
k_r2_A = k_r2*0; 

kr_r3_A = kr_r3*0;
Tr_r3_A = Tr_r3; 
Ea_r3_A = Ea_r3; 

kr_r5_A = kr_r5*0;
Tr_r5_A = Tr_r5; 
Ea_r5_A = Ea_r5; 

kr_r6_A = kr_r6*0;
Tr_r6_A = Tr_r6; 
Ea_r6_A = Ea_r6; 

kr_r7_A = kr_r7*0;
Tr_r7_A = Tr_r7; 
Ea_r7_A = Ea_r7; 

% FeS2(s) + 7/2O2(a) + H2O(l) => FeSO4(a) + H2SO4(a)
klr_r8_A = klr_r8*1.0; % FeS2 linear leaching (shrinkage) rate (m/s) @ Tr
Tr_r8_A = Tr_r8; 
Ea_r8_A = Ea_r8; 

kr_r9_A = kr_r9*0;
Tr_r9_A = Tr_r9; 
Ea_r9_A = Ea_r9; 

k_r14_A = k_r14*0; 

k_r16_A = k_r16*1e+2; % H2O(l) => H2O(g)

k_r20_A = k_r20*0; 

kr_r21_A = kr_r21*1; % FeSO4(a) + 1/4O2(a) + 1/2H2SO4(a) => 1/2Fe2(SO4)3(a) + 1/2H2O(l)
Tr_r21_A = Tr_r21;
Ea_r21_A = Ea_r21;

k_r22_A = k_r22*1; % Fe2(SO4)3(a) + 3H2O(l) <=> Fe2O3(s) + 3H2SO4(a)
k_r23_A = k_r23*1; % Fe2(SO4)3(a) + 2H2O(l) <=> 2Fe(OH)SO4(s) + H2SO4(a)

kr_r25_A = kr_r25; % O2(g) => O2(a)

% Batch Autoclave
Vt_BA = 7.5e-3; % Total volume (m^3)
V_sl_oG_BA = 7.0e-3; % Overflow volume (m^3)
L_sl_oG_BA = 7.0e-3/(1/4*pi*0.15^2); % Overflow level (m)

pD_sl_BA = 0.025; % Slurry phase pipe diameters (m)
Kf_sl_BA = 100; % Slurry phase friction factor 

pD_g_BA = 0.010; % Gas phase pipe diameters (m)
Kf_g_BA = 1; % Gas phase friction factor 

% Pilot Autoclave
Vt_A1 = 8.5e-3; % Total volume (m^3)
V_sl_oG_A1 = 5.0e-3; % Overflow volume (m^3)
L_sl_oG_A1 = 0.30; % Overflow level (m)

Vt_A2 = 8.5e-3; % Total volume (m^3)
V_sl_oG_A2 = 5.0e-3; % Overflow volume (m^3)
L_sl_oG_A2 = 0.30; % Overflow level (m)

Vt_A3 = 8.5e-3; % Total volume (m^3)
V_sl_oG_A3 = 5.0e-3; % Overflow volume (m^3)
L_sl_oG_A3 = 0.30; % Overflow level (m)

Vt_A4 = 8.5e-3; % Total volume (m^3)
V_sl_oG_A4 = 5.0e-3; % Overflow volume (m^3)
L_sl_oG_A4 = 0.30; % Overflow level (m)

pD_sl_A = 0.025; % Slurry phase pipe diameters (m)
Kf_sl_A = 10; % Slurry phase friction factor 

pD_g_A = 0.025; % Gas phase pipe diameters (m)
Kf_g_A = 1; % Gas phase friction factor 


%% Modulating Valves
% Pilot autoclave
Vt_MV1 = 1e-4; % Total valve volume (m^3)
pD_MV1 = 0.020; % Vent pipe diameter (m)
Kv50_MV1 = 5e+7; % Valve friction loss coefficient at 50% position
Kv100_MV1 = 1e+6; % Valve friction loss coefficient at 100% position


%% Vent Valves
% Batch autoclave
pD_VV1 = 0.010; % Vent pipe diameter (m)
Kv50_VV1 = 5e+6; % Valve friction loss coefficient at 50% position
Kv100_VV1 = 1e+5; % Valve friction loss coefficient at 100% position

% Pilot autoclave
pD_VV2 = 0.025; % Vent pipe diameter (m)
Kv50_VV2 = 5e+7; % Valve friction loss coefficient at 50% position
Kv100_VV2 = 1e+6; % Valve friction loss coefficient at 100% position

pD_VV3 = 0.025; % Vent pipe diameter (m)
Kv50_VV3 = 5e+2; % Valve friction loss coefficient at 50% position
Kv100_VV3 = 1e+1; % Valve friction loss coefficient at 100% position

pD_VV4 = 0.025; % Vent pipe diameter (m)
Kv50_VV4 = 5e+2; % Valve friction loss coefficient at 50% position
Kv100_VV4 = 1e+1; % Valve friction loss coefficient at 100% position


%% Slurry Valves 
% Batch autoclave
pD_SV1 = 0.0060; % Slurry pipe diameter (m) 
Kv50_SV1 = 5e+7; % Valve friction loss coefficient at 50% position 
Kv100_SV1 = 1e+6; % Valve friction loss coefficient at 100% position

% Pilot autoclave
pD_SV2 = 0.025; % Slurry pipe diameter (m)
Kv50_SV2 = 5e+7; % Valve friction loss coefficient at 50% position
Kv100_SV2 = 1e+6; % Valve friction loss coefficient at 100% position

pD_SV3 = 0.025; % Slurry pipe diameter (m)
Kv50_SV3 = 5e+2; % Valve friction loss coefficient at 50% position 
Kv100_SV3 = 1e+1; % Valve friction loss coefficient at 100% position

pD_SV4 = 0.025; % Slurry pipe diameter (m)
Kv50_SV4 = 5e+2; % Valve friction loss coefficient at 50% position 
Kv100_SV4 = 1e+1; % Valve friction loss coefficient at 100% position


%% Flash Valves
% Pilot autoclave 
Vt_FV1 = 0.10e-3; % Total valve volume (m^3)
fVt_g_FV1 = 0.5; % Fraction of flash valve volume for gas disengagement 
pD_FV1 = 0.025; % Flash pipe diameter (m)
Kf_g_FV1 = 1; %1 Gas phase combined loss coefficient 
Kv50_FV1 = 5e+7; % Valve friction loss coefficient at 50% position
Kv100_FV1 = 1e+6; % Valve friction loss coefficient at 100% position

% Reactions
k_r2_FV = k_r2*0; % So(s) <=> So(l)
k_r16_FV = 1e+3; % H2O(l) <=> H2O(g)
kr_r25_FV = kr_r25*1e-1; % O2(g) <=> O2(a)


%% Flash Drum properties
% Pilot autoclave 
Vt_FD1 = 4*8.5e-3; % Total volume (m^3)
Vt_FD2 = 8.5e-3; % Total volume (m^3)
Ab_FD1 = 1/4*pi*0.145^2; % Bottom area (m^2)

pD_i_FD1 = 0.025; % Inflow pipe diameter (m)
fpA_i_FD1 = 1.0; % Orifice opening as a fraction of the inflow pipe area
Kv50_FD1 = 5e+1; % Friction loss coefficient at 50% pipe area
Kv100_FD1 = 1e+1; % Friction loss coefficient at 100% pipe area

pD_o_FD1 = 0.025; % Outlet pipe diameters (m)
Kf_sl_o_FD1 = 1e+1; % Bottom off-take combined loss coefficient
Kf_g_o_FD1 = 1; % Gas off-take combined loss coefficient

% Reactions
k_r2_FD = k_r2*0; % So(s) => So(l)
k_r16_FD = k_r16*1e+3; % H2O(l) => H2O(g)
kr_r25_FD = kr_r25*1e-2; % O2(g) => O2(a)

