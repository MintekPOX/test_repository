
m_init_flow = 1000*reshape(simlog.Tflow_Sensor3.Flow_Sensor.dm.series.values,[],length(tout));

liq_mass_flow = m_init_flow(13:23,:);
mass_frac_flow = liq_mass_flow(:,1)./sum(liq_mass_flow(:,1));
average_dens_flow = sum(rho(13:23).*mass_frac_flow);
vol_flow = liq_mass_flow(:,1)./average_dens_flow;
conc_flow = liq_mass_flow(:,1)./(dVt.Data(1,1));



m_init = 1000*reshape(simlog.Autoclave_Pot_1.Pressure_Reactor.m_meas.series.values,[],length(tout));
liq_mass = m_init(13:23,:);
mass_frac = liq_mass(:,1)./sum(liq_mass(:,1));
average_dens = sum(rho(13:23).*mass_frac);
conc_mass = liq_mass(:,1)./average_dens;

figure;plot(Conc_pot1.Data(:,3))

species_mass_flow_P1 = reshape(simlog.Tflow_Sensor3.Flow_Sensor.dm.series.values,[],length(tout));
species_mass_flow_P2 = reshape(simlog.Tflow_Sensor5.Flow_Sensor.dm.series.values,[],length(tout));
species_mass_flow_P3 = reshape(simlog.Tflow_Sensor7.Flow_Sensor.dm.series.values,[],length(tout));
species_mass_flow_P4 = reshape(simlog.Tflow_Sensor10.Flow_Sensor.dm.series.values,[],length(tout));


figure;
plot(Conc_pot1.Data(:,3),'b'); hold on;
plot(Conc_pot2.Data(:,3),'g'); hold on;
plot(Conc_pot3.Data(:,3),'r'); hold on;
plot(Conc_pot4.Data(:,3),'y'); hold on;