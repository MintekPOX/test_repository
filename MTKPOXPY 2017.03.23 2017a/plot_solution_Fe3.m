function plot_solution_Fe3(location,time,Fe3,sTitle,sColour,sDispName)

    subplot(2,2,location);
%     figure(FigName)
    plot(time,Fe3,'Color',sColour,'Linewidth',1.5,'DisplayName',sDispName); hold on;
    xlabel('time (seconds)'); ylabel('Fe2(SO4)3 concentration (g/L)');
%     xlim([0 1000]);
    title(sTitle);  
    

end