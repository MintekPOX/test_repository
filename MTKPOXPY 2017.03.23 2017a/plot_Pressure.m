function plot_Pressure(location,time,pressure,sTitle,sColour,sDispName)
    
    subplot(2,2,location);
%     figure(FigName)
    plot(time,pressure,'Color',sColour,'Linewidth',1.5,'DisplayName',sDispName); hold on;
    ylabel('Pressure (bar)'); xlabel('time (seconds)');
%     ylim([193 206]); 
%     xlim([0 1000])
    title(sTitle);  
    
end