function plot_solution_O2(location,time,O2,sTitle,sColour,sDispName)

    subplot(2,2,location);
%     figure(FigName)
    plot(time,O2,'Color',sColour,'Linewidth',1.5,'DisplayName',sDispName); hold on;
    xlabel('time (seconds)'); ylabel('oxygen concentration (g/L)');
%     xlim([0 1000]);
    title(sTitle);  
    

end