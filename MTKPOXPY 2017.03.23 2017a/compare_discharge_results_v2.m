%% discharge investigtion
clear
clc

plantParameters_POXPY

%% example2
simlog_ex2 = simlog;

level_ex2    = simlog.Autoclave_Pot_4.Pressure_Reactor.L_meas.series.values;
pressure_ex2 = (1e-5)*simlog.Autoclave_Pot_4.Pressure_Reactor.P.series.values;

time_ex2 = tout;

temp_pot1_ex2 = simlog.Autoclave_Pot_1.Pressure_Reactor.T.series.values-273.15;
temp_pot2_ex2 = simlog.Autoclave_Pot_2.Pressure_Reactor.T.series.values-273.15;
temp_pot3_ex2 = simlog.Autoclave_Pot_3.Pressure_Reactor.T.series.values-273.15;
temp_pot4_ex2 = simlog.Autoclave_Pot_4.Pressure_Reactor.T.series.values-273.15;

pressure_pot1_ex2 = (1e-5)*simlog.Autoclave_Pot_1.Pressure_Reactor.P.series.values;
pressure_pot2_ex2 = (1e-5)*simlog.Autoclave_Pot_2.Pressure_Reactor.P.series.values;
pressure_pot3_ex2 = (1e-5)*simlog.Autoclave_Pot_3.Pressure_Reactor.P.series.values;
pressure_pot4_ex2 = (1e-5)*simlog.Autoclave_Pot_4.Pressure_Reactor.P.series.values;

O2_cons_pot1_ex2 = O2_cons_pot1.Data;
O2_cons_pot2_ex2 = O2_cons_pot2.Data;
O2_cons_pot3_ex2 = O2_cons_pot3.Data;
O2_cons_pot4_ex2 = O2_cons_pot4.Data;

solution_conc_pot1_ex2 = Conc_pot1;
solution_conc_pot2_ex2 = Conc_pot2;
solution_conc_pot3_ex2 = Conc_pot3;
solution_conc_pot4_ex2 = Conc_pot4;

extent_pot1_ex2 = Extents_A1.Data;
extent_pot2_ex2 = Extents_A2.Data;
extent_pot3_ex2 = Extents_A3.Data;
extent_pot4_ex2 = Extents_A4.Data;

modulating_valve_movement_ex2 = simlog.Modulating_Valve1.Modulating_Valve.fvai.series.values;
slurry_valve_movement_ex2 = simlog.Slurry_Valve2.Slurry_Valve.fvai.series.values;

%% example 3
level_ex3    = simlog.Autoclave_Pot_4.Pressure_Reactor.L_meas.series.values;
pressure_ex3 = (1e-5)*simlog.Autoclave_Pot_4.Pressure_Reactor.P.series.values;

time_ex3 = tout;

temp_pot1_ex3 = simlog.Autoclave_Pot_1.Pressure_Reactor.T.series.values-273.15;
temp_pot2_ex3 = simlog.Autoclave_Pot_2.Pressure_Reactor.T.series.values-273.15;
temp_pot3_ex3 = simlog.Autoclave_Pot_3.Pressure_Reactor.T.series.values-273.15;
temp_pot4_ex3 = simlog.Autoclave_Pot_4.Pressure_Reactor.T.series.values-273.15;

pressure_pot1_ex3 = (1e-5)*simlog.Autoclave_Pot_1.Pressure_Reactor.P.series.values;
pressure_pot2_ex3 = (1e-5)*simlog.Autoclave_Pot_2.Pressure_Reactor.P.series.values;
pressure_pot3_ex3 = (1e-5)*simlog.Autoclave_Pot_3.Pressure_Reactor.P.series.values;
pressure_pot4_ex3 = (1e-5)*simlog.Autoclave_Pot_4.Pressure_Reactor.P.series.values;

O2_cons_pot1_ex3 = O2_cons_pot1.Data;
O2_cons_pot2_ex3 = O2_cons_pot2.Data;
O2_cons_pot3_ex3 = O2_cons_pot3.Data;
O2_cons_pot4_ex3 = O2_cons_pot4.Data;

solution_conc_pot1_ex3 = Conc_pot1;
solution_conc_pot2_ex3 = Conc_pot2;
solution_conc_pot3_ex3 = Conc_pot3;
solution_conc_pot4_ex3 = Conc_pot4;

extent_pot1_ex3 = Extents_A1.Data;
extent_pot2_ex3 = Extents_A2.Data;
extent_pot3_ex3 = Extents_A3.Data;
extent_pot4_ex3 = Extents_A4.Data;

modulating_valve_movement_ex3 = simlog.Modulating_Valve1.Modulating_Valve.fvai.series.values;
flash_valve_movement_ex3 = simlog.Flash_Valve1.Flash_Valve.fvai.series.values;


%% example 4
level_ex4    = simlog.Autoclave_Pot_4.Pressure_Reactor.L_meas.series.values;
pressure_ex4 = (1e-5)*simlog.Autoclave_Pot_4.Pressure_Reactor.P.series.values;

time_ex4 = tout;

temp_pot1_ex4 = simlog.Autoclave_Pot_1.Pressure_Reactor.T.series.values-273.15;
temp_pot2_ex4 = simlog.Autoclave_Pot_2.Pressure_Reactor.T.series.values-273.15;
temp_pot3_ex4 = simlog.Autoclave_Pot_3.Pressure_Reactor.T.series.values-273.15;
temp_pot4_ex4 = simlog.Autoclave_Pot_4.Pressure_Reactor.T.series.values-273.15;

pressure_pot1_ex4 = (1e-5)*simlog.Autoclave_Pot_1.Pressure_Reactor.P.series.values;
pressure_pot2_ex4 = (1e-5)*simlog.Autoclave_Pot_2.Pressure_Reactor.P.series.values;
pressure_pot3_ex4 = (1e-5)*simlog.Autoclave_Pot_3.Pressure_Reactor.P.series.values;
pressure_pot4_ex4 = (1e-5)*simlog.Autoclave_Pot_4.Pressure_Reactor.P.series.values;

O2_cons_pot1_ex4 = O2_cons_pot1.Data;
O2_cons_pot2_ex4 = O2_cons_pot2.Data;
O2_cons_pot3_ex4 = O2_cons_pot3.Data;
O2_cons_pot4_ex4 = O2_cons_pot4.Data;

solution_conc_pot1_ex4 = Conc_pot1;
solution_conc_pot2_ex4 = Conc_pot2;
solution_conc_pot3_ex4 = Conc_pot3;
solution_conc_pot4_ex4 = Conc_pot4;

extent_pot1_ex4 = Extents_A1.Data;
extent_pot2_ex4 = Extents_A2.Data;
extent_pot3_ex4 = Extents_A3.Data;
extent_pot4_ex4 = Extents_A4.Data;

vent_valve1_movement_ex4 = simlog.Vent_Valve2.Vent_Valve.fvai.series.values;
vent_valve2_movement_ex4 = simlog.Vent_Valve4.Vent_Valve.fvai.series.values;
slurry_valve1_movement_ex4 = simlog.Slurry_Valve3.Slurry_Valve.fvai.series.values;
slurry_valve2_movement_ex4 = simlog.Slurry_Valve4.Slurry_Valve.fvai.series.values;

%% plot
figure;
L_SP = repmat(L_sl_oG_A4,1,length(tout));
plot(tout,L_SP,'k-','DisplayName','setpoint','Linewidth',1.5); hold on;
plot(time_ex2,level_ex2,'b','DisplayName','Ex2','Linewidth',1.5);
plot(time_ex3,level_ex3,'g','DisplayName','Ex3','Linewidth',1.5);
plot(time_ex4,level_ex4,'m','DisplayName','Ex4','Linewidth',1.5);
xlabel('Time (seconds)'); ylabel('Level (m)')
legend show

figure;
P_SP = (1e-5)*repmat(Pg+P_atm,1,length(tout));
plot(tout, P_SP,'k-','DisplayName','setpoint','Linewidth',1.5); hold on;
plot(time_ex2, pressure_ex2,'b','DisplayName','Ex2','Linewidth',1.5);
plot(time_ex3, pressure_ex3,'g','DisplayName','Ex3','Linewidth',1.5);
plot(time_ex4, pressure_ex4,'m','DisplayName','Ex4','Linewidth',1.5);
xlabel('Time (seconds)'); ylabel('Pressure (bar)')

figure;
plot_Temp(1,time_ex2,temp_pot1_ex2,'Pot1','b','Ex2');
plot_Temp(2,time_ex2,temp_pot1_ex2,'Pot2','b','Ex2');
plot_Temp(3,time_ex2,temp_pot1_ex2,'Pot3','b','Ex2');
plot_Temp(4,time_ex2,temp_pot1_ex2,'Pot4','b','Ex2');
plot_Temp(1,time_ex3,temp_pot1_ex3,'Pot1','g','Ex3');
plot_Temp(2,time_ex3,temp_pot1_ex3,'Pot2','g','Ex3');
plot_Temp(3,time_ex3,temp_pot1_ex3,'Pot3','g','Ex3');
plot_Temp(4,time_ex3,temp_pot1_ex3,'Pot4','g','Ex3');
plot_Temp(1,time_ex4,temp_pot1_ex4,'Pot1','m','Ex4');
plot_Temp(2,time_ex4,temp_pot1_ex4,'Pot2','m','Ex4');
plot_Temp(3,time_ex4,temp_pot1_ex4,'Pot3','m','Ex4');
plot_Temp(4,time_ex4,temp_pot1_ex4,'Pot4','m','Ex4');
legend show;

figure;
plot_Pressure(1,time_ex2,pressure_pot1_ex2,'Pot1','b','Ex2');
plot_Pressure(2,time_ex2,pressure_pot1_ex2,'Pot2','b','Ex2');
plot_Pressure(3,time_ex2,pressure_pot1_ex2,'Pot3','b','Ex2');
plot_Pressure(4,time_ex2,pressure_pot1_ex2,'Pot4','b','Ex2');
plot_Pressure(1,time_ex3,pressure_pot1_ex3,'Pot1','g','Ex3');
plot_Pressure(2,time_ex3,pressure_pot1_ex3,'Pot2','g','Ex3');
plot_Pressure(3,time_ex3,pressure_pot1_ex3,'Pot3','g','Ex3');
plot_Pressure(4,time_ex3,pressure_pot1_ex3,'Pot4','g','Ex3');
plot_Pressure(1,time_ex4,pressure_pot1_ex4,'Pot1','m','Ex4');
plot_Pressure(2,time_ex4,pressure_pot1_ex4,'Pot2','m','Ex4');
plot_Pressure(3,time_ex4,pressure_pot1_ex4,'Pot3','m','Ex4');
plot_Pressure(4,time_ex4,pressure_pot1_ex4,'Pot4','m','Ex4');
legend show;

figure;
plot_O2_consumption(1,time_ex2,O2_cons_pot1_ex2,'Pot1','b','Ex2');
plot_O2_consumption(2,time_ex2,O2_cons_pot1_ex2,'Pot2','b','Ex2');
plot_O2_consumption(3,time_ex2,O2_cons_pot1_ex2,'Pot3','b','Ex2');
plot_O2_consumption(4,time_ex2,O2_cons_pot1_ex2,'Pot4','b','Ex2');
plot_O2_consumption(1,time_ex3,O2_cons_pot1_ex3,'Pot1','g','Ex3');
plot_O2_consumption(2,time_ex3,O2_cons_pot1_ex3,'Pot2','g','Ex3');
plot_O2_consumption(3,time_ex3,O2_cons_pot1_ex3,'Pot3','g','Ex3');
plot_O2_consumption(4,time_ex3,O2_cons_pot1_ex3,'Pot4','g','Ex3');
plot_O2_consumption(1,time_ex4,O2_cons_pot1_ex4,'Pot1','m','Ex4');
plot_O2_consumption(2,time_ex4,O2_cons_pot1_ex4,'Pot2','m','Ex4');
plot_O2_consumption(3,time_ex4,O2_cons_pot1_ex4,'Pot3','m','Ex4');
plot_O2_consumption(4,time_ex4,O2_cons_pot1_ex4,'Pot4','m','Ex4');
legend show;

figure;
plot_solution_O2(1,time_ex2,solution_conc_pot1_ex2.Data(:,3),'Pot1','b','Ex2')
plot_solution_O2(2,time_ex2,solution_conc_pot2_ex2.Data(:,3),'Pot2','b','Ex2')
plot_solution_O2(3,time_ex2,solution_conc_pot3_ex2.Data(:,3),'Pot3','b','Ex2')
plot_solution_O2(4,time_ex2,solution_conc_pot4_ex2.Data(:,3),'Pot4','b','Ex2')
plot_solution_O2(1,time_ex3,solution_conc_pot1_ex3.Data(:,3),'Pot1','g','Ex3')
plot_solution_O2(2,time_ex3,solution_conc_pot2_ex3.Data(:,3),'Pot2','g','Ex3')
plot_solution_O2(3,time_ex3,solution_conc_pot3_ex3.Data(:,3),'Pot3','g','Ex3')
plot_solution_O2(4,time_ex3,solution_conc_pot4_ex3.Data(:,3),'Pot4','g','Ex3')
plot_solution_O2(1,time_ex4,solution_conc_pot1_ex4.Data(:,3),'Pot1','m','Ex4')
plot_solution_O2(2,time_ex4,solution_conc_pot2_ex4.Data(:,3),'Pot2','m','Ex4')
plot_solution_O2(3,time_ex4,solution_conc_pot3_ex4.Data(:,3),'Pot3','m','Ex4')
plot_solution_O2(4,time_ex4,solution_conc_pot4_ex4.Data(:,3),'Pot4','m','Ex4')
legend show;

figure;
plot_solution_Fe2(1,time_ex2,solution_conc_pot1_ex2.Data(:,9),'Pot1','b','Ex2')
plot_solution_Fe2(2,time_ex2,solution_conc_pot2_ex2.Data(:,9),'Pot2','b','Ex2')
plot_solution_Fe2(3,time_ex2,solution_conc_pot3_ex2.Data(:,9),'Pot3','b','Ex2')
plot_solution_Fe2(4,time_ex2,solution_conc_pot4_ex2.Data(:,9),'Pot4','b','Ex2')
plot_solution_Fe2(1,time_ex3,solution_conc_pot1_ex3.Data(:,9),'Pot1','g','Ex3')
plot_solution_Fe2(2,time_ex3,solution_conc_pot2_ex3.Data(:,9),'Pot2','g','Ex3')
plot_solution_Fe2(3,time_ex3,solution_conc_pot3_ex3.Data(:,9),'Pot3','g','Ex3')
plot_solution_Fe2(4,time_ex3,solution_conc_pot4_ex3.Data(:,9),'Pot4','g','Ex3')
plot_solution_Fe2(1,time_ex4,solution_conc_pot1_ex4.Data(:,9),'Pot1','m','Ex4')
plot_solution_Fe2(2,time_ex4,solution_conc_pot2_ex4.Data(:,9),'Pot2','m','Ex4')
plot_solution_Fe2(3,time_ex4,solution_conc_pot3_ex4.Data(:,9),'Pot3','m','Ex4')
plot_solution_Fe2(4,time_ex4,solution_conc_pot4_ex4.Data(:,9),'Pot4','m','Ex4')
legend show;

figure;
plot_solution_Fe3(1,time_ex2,solution_conc_pot1_ex2.Data(:,10),'Pot1','b','Ex2')
plot_solution_Fe3(2,time_ex2,solution_conc_pot2_ex2.Data(:,10),'Pot2','b','Ex2')
plot_solution_Fe3(3,time_ex2,solution_conc_pot3_ex2.Data(:,10),'Pot3','b','Ex2')
plot_solution_Fe3(4,time_ex2,solution_conc_pot4_ex2.Data(:,10),'Pot4','b','Ex2')
plot_solution_Fe3(1,time_ex3,solution_conc_pot1_ex3.Data(:,10),'Pot1','g','Ex3')
plot_solution_Fe3(2,time_ex3,solution_conc_pot2_ex3.Data(:,10),'Pot2','g','Ex3')
plot_solution_Fe3(3,time_ex3,solution_conc_pot3_ex3.Data(:,10),'Pot3','g','Ex3')
plot_solution_Fe3(4,time_ex3,solution_conc_pot4_ex3.Data(:,10),'Pot4','g','Ex3')
plot_solution_Fe3(1,time_ex4,solution_conc_pot1_ex4.Data(:,10),'Pot1','m','Ex4')
plot_solution_Fe3(2,time_ex4,solution_conc_pot2_ex4.Data(:,10),'Pot2','m','Ex4')
plot_solution_Fe3(3,time_ex4,solution_conc_pot3_ex4.Data(:,10),'Pot3','m','Ex4')
plot_solution_Fe3(4,time_ex4,solution_conc_pot4_ex4.Data(:,10),'Pot4','m','Ex4')
legend show;

figure;
plot_solution_acid(1,time_ex2,solution_conc_pot1_ex2.Data(:,5),'Pot1','b','Ex2')
plot_solution_acid(2,time_ex2,solution_conc_pot2_ex2.Data(:,5),'Pot2','b','Ex2')
plot_solution_acid(3,time_ex2,solution_conc_pot3_ex2.Data(:,5),'Pot3','b','Ex2')
plot_solution_acid(4,time_ex2,solution_conc_pot4_ex2.Data(:,5),'Pot4','b','Ex2')
plot_solution_acid(1,time_ex3,solution_conc_pot1_ex3.Data(:,5),'Pot1','g','Ex3')
plot_solution_acid(2,time_ex3,solution_conc_pot2_ex3.Data(:,5),'Pot2','g','Ex3')
plot_solution_acid(3,time_ex3,solution_conc_pot3_ex3.Data(:,5),'Pot3','g','Ex3')
plot_solution_acid(4,time_ex3,solution_conc_pot4_ex3.Data(:,5),'Pot4','g','Ex3')
plot_solution_acid(1,time_ex4,solution_conc_pot1_ex4.Data(:,5),'Pot1','m','Ex4')
plot_solution_acid(2,time_ex4,solution_conc_pot2_ex4.Data(:,5),'Pot2','m','Ex4')
plot_solution_acid(3,time_ex4,solution_conc_pot3_ex4.Data(:,5),'Pot3','m','Ex4')
plot_solution_acid(4,time_ex4,solution_conc_pot4_ex4.Data(:,5),'Pot4','m','Ex4')
legend show;

figure;
plot_extents(1,time_ex2,extent_pot1_ex2,'Pot1','b','Ex2');
plot_extents(2,time_ex2,extent_pot2_ex2,'Pot2','b','Ex2');
plot_extents(3,time_ex2,extent_pot3_ex2,'Pot3','b','Ex2');
plot_extents(4,time_ex2,extent_pot4_ex2,'Pot4','b','Ex2');
plot_extents(1,time_ex3,extent_pot1_ex3,'Pot1','b','Ex3');
plot_extents(2,time_ex3,extent_pot2_ex3,'Pot2','b','Ex3');
plot_extents(3,time_ex3,extent_pot3_ex3,'Pot3','b','Ex3');
plot_extents(4,time_ex3,extent_pot4_ex3,'Pot4','b','Ex3');
plot_extents(1,time_ex4,extent_pot1_ex4,'Pot1','b','Ex4');
plot_extents(2,time_ex4,extent_pot2_ex4,'Pot2','b','Ex4');
plot_extents(3,time_ex4,extent_pot3_ex4,'Pot3','b','Ex4');
plot_extents(4,time_ex4,extent_pot4_ex4,'Pot4','b','Ex4');
legend show;

figure;
subplot(3,1,1);plot(time_ex2,modulating_valve_movement_ex2,time_ex2,slurry_valve_movement_ex2);
subplot(3,1,2);plot(time_ex3,modulating_valve_movement_ex3,time_ex3,flash_valve_movement_ex3)
subplot(3,1,3);plot(time_ex4,vent_valve1_movement_ex4,time_ex4,vent_valve2_movement_ex4,time_ex4,slurry_valve1_movement_ex4,time_ex4,slurry_valve2_movement_ex4)

